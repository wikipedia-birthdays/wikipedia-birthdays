# wikipedia-birthdays
ever wanted to know what famous people shared your birthday (march 4th) but 
didn't want to go through all the not-so-famous famous people on wikipedia? 
wanted to memorize the birthdays of the 4000 most relevant people (according to 
wikipedia)? well now you can! with this simple program (or set of programs), 
you can now find something marginally more productive than doomscrolling social 
media

i wrote most of these files (this readme included!) while very very high on
caffeine please excuse me

## usage
i'm not too sure how this codeberg thing works. i think you go to 
https://wikipedia-birthdays.codeberg.page/wikipedia-birthdays/site/ or
something of the sorts (if you just want to use the final product)

the rest of the files in this repository are tools to help create the final
product (i.e. the one `data.js` file). although these tools will do 90% of the
work, there is still some manual tweeking to do.

these tools were basically made for myself, which may make it difficult to use.
if you don't understand how to use these programs but kinda want to use it 
anyways, feel free to plonk an issue (or two) over at 
https://codeberg.org/wikipedia-birthdays/wikipedia-birthdays/issues and we can
figure it out together.

i'll try to improve these tools over-time. i'm new to some of the technologies
used in this repository (i.e. rust, bootstrap) so i don't expect the code to be
perfect (it was mostly rushed anyways).

## license
all code in this repository is licensed under 
[the unlicense](https://unlicense.org), which (i think) pretty much means you 
can do anything you want with it, although given how messy and unclean
everything is i'm not sure why you'd want to use it in the first place.

this project uses many code dependencies, including (but not limited to):
- [AHash](https://github.com/tkaitchuck/ahash) (mit + apache 2.0 license)
- [clap](https://github.com/clap-rs/clap) (mit + apache 2.0 license)
- [Click](https://github.com/pallets/click/) (bsd 3-clause license)
- [flate2](https://github.com/rust-lang/flate2-rs) (mit + apache 2.0 license)
- [Serde](https://github.com/serde-rs/serde) (mit + apache 2.0 license)
- [Serde JSON](https://github.com/serde-rs/json) (mit + apache 2.0 license)
- [utf8-decode](https://github.com/timothee-haudebourg/utf8-decode) 
  (mit + apache 2.0 license)



