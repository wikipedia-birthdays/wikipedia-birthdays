from pathlib import Path
import bisect
import json

import click

MONTHS = {
    "January": 31,
    "February": 29,
    "March": 31,
    "April": 30,
    "May": 31,
    "June": 30,
    "July": 31,
    "August": 31,
    "September": 30,
    "October": 31,
    "November": 30,
    "December": 31,
}

DAY_VALUES = {}
index = 0
for month, days_in_month in MONTHS.items():
    month = month.lower()
    for day in range(1, days_in_month+1):
        DAY_VALUES[f"{month}-{day}"] = index
        index += 1


def key(birthday: dict[str, str]) -> tuple[int, int, int]:
    day_value = int(DAY_VALUES[birthday["date"]])
    is_bc = "BC" in birthday["year"]
    year_value = int(birthday["year"].replace("BC", "")) * (-1 if is_bc else 1)
    link_value = -int(birthday["count"])
    return day_value, year_value, link_value


@click.command()
@click.argument("birthdays-file")
@click.argument("people-links-file")
@click.argument("final-destination-file")
@click.option("--max-birthdays", default=-1)
@click.option("--min-links", default=0)
def main(
            birthdays_file: str,
            people_links_file: str,
            final_destination_file: str,
            max_birthdays: int,
            min_links: int,
        ) -> None:
    birthdays_file_path = Path(birthdays_file)
    people_links_file_path = Path(people_links_file)
    final_destination_path = Path(final_destination_file)

    birthday_dicts = {}
    birthdays = []

    with open(people_links_file_path, "r") as f:
        for index, line in enumerate(f):
            if max_birthdays > 0 and index >= max_birthdays:
                break

            data = json.loads(line)
            if data["count"] < min_links:
                break

            birthday_dicts[data["page_title"]] = data

    with open(birthdays_file_path, "r") as f:
        for line in f:
            data = json.loads(line)
            if data["page_title"] not in birthday_dicts:
                continue

            data["date"] = data["date"].lower().replace("_", "-")
            birthday_dicts[data["page_title"]].update(data)

            bisect.insort(birthdays, birthday_dicts[data["page_title"]], key=key)

    with open(final_destination_path, "w") as f:
        f.write("const birthdays = [\n")
        for birthday in birthdays:
            page_title = birthday["page_title"]
            date = birthday["date"]
            year = birthday["year"]
            description = birthday["description"]
            link_count = birthday["count"]

            # this code will probably lead to the world's first javascript
            # injection attack
            f.write(f"  ")
            f.write(f"new Map([")
            f.write(f"['page_title', {page_title!r}], ")
            f.write(f"['date', {date!r}], ")
            f.write(f"['year', {year!r}], ")
            f.write(f"['description', {description!r}], ")
            f.write(f"['link_count', {link_count!r}]")
            f.write(f"]),\n")

        f.write("];")


if __name__ == "__main__":
    main()
