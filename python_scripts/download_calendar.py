from pathlib import Path
import click
import requests
import time

MONTHS = {
    "January": 31,
    "February": 29,
    "March": 31,
    "April": 30,
    "May": 31,
    "June": 30,
    "July": 31,
    "August": 31,
    "September": 30,
    "October": 31,
    "November": 30,
    "December": 31,
}

RATE_LIMIT = 1
TITLES_PER_REQUEST = 50

API_URI = r"https://en.wikipedia.org/w/api.php"

PAGE_DOWNLOAD_PARAMS = {
    "action": "query",
    "prop": "revisions",
    "rvslots": "*",
    "rvprop": "content",
    "format": "json",
    "formatversion": 2,
}


@click.command()
@click.argument("saved_dates_directory")
def main(saved_dates_directory: str) -> None:
    saved_dates_directory_path = Path(saved_dates_directory)
    saved_dates_directory_path.mkdir(parents=True, exist_ok=True)

    dates = []
    for month, days in MONTHS.items():
        for day in range(1, days+1):
            dates.append(f"{month}_{day}".title())

    index = 0

    while dates[index:]:
        params = PAGE_DOWNLOAD_PARAMS.copy()
        params["titles"] = "|".join(dates[index:index+TITLES_PER_REQUEST])

        data = requests.get(API_URI, params=params).json()
        for page in data["query"]["pages"]:
            title = page["title"].replace(" ", "_")
            with open(saved_dates_directory_path / f"{title}.html", "w") as f:
                f.write(page["revisions"][0]["slots"]["main"]["content"])

            index += 1

        print(f"{index}/{len(dates)} processed ({(index)/len(dates):.2%})")
        print(f"Processed {dates[index-1]}")
        print()
        time.sleep(RATE_LIMIT)


if __name__ == "__main__":
    main()
