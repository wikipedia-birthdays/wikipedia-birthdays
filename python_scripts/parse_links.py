"""
this python script is very slow, use the rust script (of the same name) instead
"""

from collections.abc import Iterable, Iterator
from io import SEEK_END, SEEK_SET, TextIOBase
from pathlib import Path
import click
import gzip
import json

LINE_PREFIX = "INSERT INTO `pagelinks` VALUES "


def char_iterator(file: TextIOBase) -> Iterator[str]:
    while True:
        char = file.read(1)

        if char:
            yield char
        else:
            return


def char_iterator_to_newline(file: TextIOBase) -> Iterator[str]:
    for char in char_iterator(file):
        if char != "\n":
            yield char
        else:
            return


def value_tuple_strings(file: TextIOBase) -> Iterator[Iterable[str]]:
    skip_to_newline = False
    column_number = 0

    previous_index = 0

    for char in char_iterator(file):
        if char == "\n":
            skip_to_newline = False
            column_number = 0
        elif skip_to_newline:
            continue
        elif column_number < len(LINE_PREFIX):
            if char != LINE_PREFIX[column_number]:
                skip_to_newline = True

            column_number += 1
            previous_index = file.tell()
        else:
            file.seek(previous_index, SEEK_SET)
            yield char_iterator_to_newline(file)
            column_number = 0


def parse_page_titles(value_tuple_string: Iterable[str]) -> Iterator[str]:
    buffer = ""
    in_string = False
    is_escaped = False

    for char in value_tuple_string:
        if char == "\n":
            break
        elif is_escaped:
            buffer += char
            is_escaped = False
        elif not in_string and char == "'":
            in_string = True
        elif in_string and char == "'":
            in_string = False

            yield buffer

            buffer = ""
        elif in_string and char == "\\":
            is_escaped = True
        elif in_string:
            buffer += char


@click.command()
@click.argument("pagelinks-file")
@click.argument("birthdays-file")
@click.argument("people-links-file")
def main(
            pagelinks_file: str,
            birthdays_file: str,
            people_links_file: str
        ) -> None:
    pagelinks_file_path = Path(pagelinks_file)
    birthdays_file_path = Path(birthdays_file)
    people_links_file_path = Path(people_links_file)

    with open(birthdays_file_path, "r") as f:
        links = {json.loads(line)["page_title"]: 0 for line in f}

    with open(pagelinks_file_path, "rb") as f:
        f.seek(0, SEEK_END)
        filesize = f.tell()
        f.seek(0, SEEK_SET)

        gzip_file = gzip.open(f, "rt")
        assert isinstance(gzip_file, TextIOBase)

        last_reported_percentage = 0

        for value_tuple_string in value_tuple_strings(gzip_file):
            for page_title in parse_page_titles(value_tuple_string):
                if page_title in links:
                    links[page_title] += 1

            print("True!")

    with open(people_links_file_path, "w") as f:
        f.write(json.dumps(links))


if __name__ == "__main__":
    main()
