from pathlib import Path
import click
import json

MONTHS = {
    "January": 31,
    "February": 29,
    "March": 31,
    "April": 30,
    "May": 31,
    "June": 30,
    "July": 31,
    "August": 31,
    "September": 30,
    "October": 31,
    "November": 30,
    "December": 31,
}


def clean_links(wikitext: str) -> str:
    out = ""
    buffer = ""
    in_link = False

    for char in wikitext:
        if not in_link and char == "[":
            buffer += "["
            if buffer.startswith("[["):
                buffer = ""
                in_link = True
        elif in_link and char == "]":
            buffer += "]"
            if buffer.endswith("]]"):
                out += buffer.removesuffix("]]")
                buffer = ""
                in_link = False
        elif in_link and char == "|":
            buffer = ""
        elif in_link:
            buffer += char
        else:
            out += char

    return out + buffer


def clean_refs(wikitext: str) -> str:
    out = ""
    buffer = ""
    in_ref = False

    for char in wikitext:
        if not in_ref:
            for i, c in reversed(list(enumerate("<ref"))):
                if buffer == "<ref"[:i] and char == c:
                    buffer += char
                    break
            else:
                out += buffer
                out += char
                buffer = ""

            if buffer == "<ref":
                buffer = ""
                in_ref = True
                continue

        elif in_ref:
            for i, c in reversed(list(enumerate("</ref>"))):
                if buffer == "</ref>"[:i] and char == c:
                    buffer += char
                    break
            else:
                buffer = ""

            if buffer == "</ref>":
                buffer = ""
                in_ref = False
                continue

    return out + buffer


def clean_wikitext(wikitext: str) -> str:
    return clean_links(clean_refs(wikitext))


def parse_line(line: str, date: str) -> dict:
    # Example format:
    #   *[[1941]] &ndash; [[Bob Wilson (footballer, born 1941)|Bob Wilson]],
    #   English footballer and sportscaster

    # # i love visual editor edits (these are technically formatted incorrectly
    # # but nobody checks them so they still exist)
    # # actually don't use these because stuff like "marie-thérèse letablier"
    # # is formatted wrong
    # line = line.replace("–", "&ndash;")
    # line = line.replace("-", "&ndash;")
    # Shout out to Lucretius's birthday for making me put this here
    line = clean_refs(line)

    # Take the part before the dash and filter of the "*[]" characters
    year = line.split("&ndash;")[0]
    year = "".join(char for char in year if char not in "*[]").strip()

    # Take the first link after the dash and take the part after the pipe,
    # if there is one
    page_title = (line.split("&ndash;")[1]
                      .split("[[")[1]
                      .split("]]")[0]
                      .split("|")[0]
                      .replace(" ", "_"))

    description = clean_wikitext(line.split("&ndash;")[1]).strip()

    return {
        "date": date,
        "year": year,
        "page_title": page_title,
        "description": description,
    }


def parse_document(document: str, date: str) -> list[dict]:
    births_section = False
    out = []

    for line in document.splitlines(keepends=True):
        # i give up. this should handle any sane case
        if line.startswith("==") and "births" in line.lower():
            births_section = True
        elif line.startswith("==") and "deaths" in line.lower():
            births_section = False
        elif line.startswith("*") and line != "*\n" and births_section:
            out.append(parse_line(line, date))

    return out


@click.command()
@click.argument("saved-dates-directory")
@click.argument("birthdays-file")
def main(saved_dates_directory: str, birthdays_file: str) -> None:
    saved_dates_directory_path = Path(saved_dates_directory)
    birthdays_file_path = Path(birthdays_file)
    birthdays_file_path.parent.mkdir(parents=True, exist_ok=True)

    dates = []
    for month, days in MONTHS.items():
        for day in range(1, days+1):
            dates.append(f"{month}_{day}".title())

    with open(birthdays_file_path, "w") as f:
        pass

    for index, date in enumerate(dates):
        with open(saved_dates_directory_path / f"{date}.html", "r") as f:
            birthdays = parse_document(f.read(), date)

        with open(birthdays_file_path, "a") as f:
            for birthday in birthdays:
                f.write(f"{json.dumps(birthday)}\n")

        print(f"{index+1}/{len(dates)} processed ({(index+1)/len(dates):.2%})")
        print(f"Processed {dates[index]}")
        print()


if __name__ == "__main__":
    main()
