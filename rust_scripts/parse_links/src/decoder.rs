use std::io::{Result, Error, ErrorKind};
use std::convert::TryFrom;

fn process_first_byte(result: Option<Result<u8>>) -> Result<Option<u32>> {
    match result {
        Some(Ok(val)) => Ok(Some(val as u32)),
        Some(Err(e))  => Err(e),
        None => Ok(None),
    }
}

fn process_continuation_byte(result: Option<Result<u8>>) -> Result<u32> {
    match result {
        Some(Ok(c)) => {
            if c & 0xC0 == 0x80 {
                Ok((c & 0x3F) as u32)
            } else {
                Err(Error::new(ErrorKind::InvalidData, "invalid UTF-8 sequence."))
            }
        },
        Some(Err(e)) => Err(e),
        None => Err(Error::new(ErrorKind::UnexpectedEof, "unexpected end of UTF-8 sequence."))
    }
}

fn raw_decode_from<I: Iterator<Item=Result<u8>>>(
    iter: &mut I
) -> Result<Option<u32>> {
    let a = match process_first_byte(iter.next()) {
        Ok(Some(val)) => val,
        Ok(None)      => return Ok(None),
        Err(e)        => return Err(e),
    };

    if a & 0x80 == 0x00 {
        Ok(Some(a))
    } else if a & 0xE0 == 0xC0 {
        let b = process_continuation_byte(iter.next())?;
        Ok(Some((a & 0x1F) << 6 | b))
    } else if a & 0xF0 == 0xE0 {
        let b = process_continuation_byte(iter.next())?;
        let c = process_continuation_byte(iter.next())?;
        Ok(Some((a & 0x0F) << 12 | b << 6 | c))
    } else if a & 0xF8 == 0xF0 {
        let b = process_continuation_byte(iter.next())?;
        let c = process_continuation_byte(iter.next())?;
        let d = process_continuation_byte(iter.next())?;
        Ok(Some((a & 0x07) << 18 | b << 12 | c << 6 | d))
    } else {
        Err(Error::new(ErrorKind::InvalidData, "invalid UTF-8 sequence."))
    }
}

fn decode_from<I: Iterator<Item=Result<u8>>>(
    iter: &mut I
) -> Result<Option<char>> {
    match raw_decode_from(iter)? {
        None      => Ok(None),
        Some(val) => match char::try_from(val) {
            Ok(val) => Ok(Some(val)),
            Err(_)  => Err(Error::new(ErrorKind::InvalidData, "invalid UTF-8 sequence."))
        }
    }
}

pub fn decode_unsafe<I: Iterator<Item=Result<u8>>>(
    iter: &mut I
) -> Option<Result<char>> {
    match decode_from(iter) {
        Ok(Some(val)) => Some(Ok(val)),
        Ok(None)      => None,
        Err(e)        => Some(Err(e)),
    }
}

pub struct UnsafeDecoder<R: Iterator<Item=Result<u8>>> {
	bytes: R
}

impl<R: Iterator<Item=Result<u8>>> UnsafeDecoder<R> {
    pub fn new(source: R) -> UnsafeDecoder<R> {
		UnsafeDecoder {
			bytes: source
		}
	}
}

impl<R: Iterator<Item=Result<u8>>> Iterator for UnsafeDecoder<R> {
	type Item = Result<char>;

	fn next(&mut self) -> Option<Result<char>> {
    	decode_unsafe(&mut self.bytes)
	}
}
