use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Error, Read, Result, Write};
use std::iter::Peekable;
use std::path::PathBuf;

use ahash::AHashMap;
use clap::Parser;
use flate2::bufread::MultiGzDecoder;
use serde::{Deserialize, Serialize};

mod decoder;
use decoder::UnsafeDecoder;

const LINE_PREFIX: &str = "INSERT INTO `pagelinks` VALUES ";

#[derive(Parser)]
struct Args {
    #[arg(required = true)]
    pagelinks_file: PathBuf,

    #[arg(required = true)]
    birthdays_file: PathBuf,

    #[arg(required = true)]
    people_links_file: PathBuf,
}

#[derive(Serialize, Deserialize)]
struct Birthday {
    date: String,
    year: String,
    page_title: String,
    description: String,
}

#[derive(Serialize, Deserialize)]
struct Link {
    page_title: String,
    count: usize,
}

fn skip_to_newline(
    peekable: &mut Peekable<impl Iterator<Item=Result<char>>>
) -> Result<bool> {
    while peekable.peek().is_some() {
        let value = match (*peekable.peek().unwrap()).as_ref() {
            Ok(val) => val,
            Err(e)  => return Err(Error::new(e.kind(), e.to_string())),
        };

        if value != &'\n' {
            peekable.next();
        }
        else {
            return Ok(true);
        }
    };

    Ok(false)
}

fn skip_to_values (
    peekable: &mut Peekable<impl Iterator<Item=Result<char>>>
) -> Result<bool> {
    // use a state machine to match the start of each line against 
    // `LINE_PREFIX`.
    let mut column_index = 0;

    while peekable.peek().is_some() {
        if column_index >= LINE_PREFIX.len() {
            return Ok(true)
        }

        let value = match (*peekable.peek().unwrap()).as_ref() {
            Ok(val) => *val,
            Err(e)  => return Err(Error::new(e.kind(), e.to_string())),
        };

        let matching_char = LINE_PREFIX.as_bytes()[column_index].into();
        
        if value == '\n' {
            column_index = 0;
            peekable.next();
            continue;
        }
        else if value != matching_char {
            column_index = 0;
            skip_to_newline(peekable)?;
            continue;
        }

        peekable.next();
        column_index += 1;
    }

    Ok(false)
}

fn extract_string(
    peekable: &mut Peekable<impl Iterator<Item=Result<char>>>
) -> Result<Option<String>> {
    let mut out = String::new();

    while peekable.peek().is_some() {
        let value = match (*peekable.peek().unwrap()).as_ref() {
            Ok(val) => *val,
            Err(e)  => return Err(Error::new(e.kind(), e.to_string())),
        };

        if value == '\n' {
            return Ok(None);
        }
        else if value == '\'' {
            peekable.next();
            break;
        }
        else {
            peekable.next();
        }
    }

    while peekable.peek().is_some() {
        let value = match (*peekable.peek().unwrap()).as_ref() {
            Ok(val) => *val,
            Err(e)  => return Err(Error::new(e.kind(), e.to_string())),
        };

        if value == '\n' {
            return Ok(None);
        }
        else if value == '\'' {
            peekable.next();
            return Ok(Some(out));
        }
        else if value == '\\' {
            // skip the first value, push the second value

            peekable.next();
            out.push(peekable.next().unwrap()?);
        }
        else {
            // since `value == peekable.next().unwrap()?`, we could also use 
            // `out.push(value)`

            out.push(peekable.next().unwrap()?);
        }
    };

    // either string was never opened or string was never closed
    Ok(None)
}


fn initialize_people_links(
    birthdays_path: &PathBuf,
    people_link_counts: &mut AHashMap<String, usize>
) -> Result<()> {
    let birthdays_file = File::open(birthdays_path)?;
    let birthdays_reader = BufReader::new(birthdays_file);
    for line in birthdays_reader.lines() {
        let birthday: Birthday = serde_json::from_str(&line?)?;
        people_link_counts.insert(birthday.page_title, 0);
    }

    Ok(())
}


fn save_people_links(
    people_links_path: &PathBuf,
    people_link_counts: &mut AHashMap<String, usize>
) -> Result<()> {
    let people_links_file = File::create(people_links_path)?;
    let mut people_links_writer = BufWriter::new(people_links_file);

    let mut sorted_links: Vec<Link> = 
        people_link_counts.iter()
                          .map(|a| Link {page_title: a.0.to_string(), count: *a.1})
                          .collect();
    sorted_links.sort_by(|a, b| b.count.cmp(&a.count));

    for link in sorted_links {
        serde_json::to_writer(&mut people_links_writer, &link)?;
        write!(people_links_writer, "\n")?;
    }

    Ok(())
}


fn main() -> Result<()>{
    let args = Args::parse();

    let mut people_link_counts = AHashMap::new();

    let pagelinks_path = args.pagelinks_file;
    let birthdays_path = args.birthdays_file;
    let people_links_path = args.people_links_file;

    initialize_people_links(&birthdays_path, &mut people_link_counts)?;

    let pagelinks_file = File::open(pagelinks_path)?;
    let pagelinks_buffer = BufReader::new(pagelinks_file);
    let pagelinks_decoder = MultiGzDecoder::new(pagelinks_buffer);
    let pagelinks_reader = UnsafeDecoder::new(pagelinks_decoder.bytes());

    let mut peek_reader = pagelinks_reader.peekable();
    let mut total_count = 0;

    while skip_to_values(&mut peek_reader)? {
        while let Some(extracted_string) = extract_string(&mut peek_reader)? {
            if !people_link_counts.contains_key(&extracted_string) {
                continue;
            }

            let count = people_link_counts.get(&extracted_string).unwrap();
            people_link_counts.insert(extracted_string, count + 1);

            total_count += 1;

            if total_count % 10_000 == 0 {
                save_people_links(&people_links_path, &mut people_link_counts)?;
            }
        }
        println!("{}", total_count);
    }

    Ok(())
}
