const MONTHS = new Map([
  ["january", 31],
  ["february", 29],
  ["march", 31],
  ["april", 30],
  ["may", 31],
  ["june", 30],
  ["july", 31],
  ["august", 31],
  ["september", 30],
  ["october", 31],
  ["november", 30],
  ["december", 31],
]);

const ALL_DAYS = [];
for (const [month, days_in_month] of MONTHS.entries()) {
  for (let day = 1; day <= days_in_month; day++) {
    ALL_DAYS.push(`${month}-${day}`);
  }
}

function parseHash(hash) {
  if (hash[0] !== "#") {
    return;
  }

  const date = hash.slice(1)
  if (!ALL_DAYS.includes(date)) {
    return;
  }

  const parts = date.split("-");
  return [parts[0], parts[1]]
}

function loadFromHash() {
  const parsedHash = parseHash(location.hash);
  if (parsedHash === undefined) {
    return;
  }

  const [month, day] = parsedHash;

  // this skips the show animation of an element
  const monthCollapsible = document.getElementById(month);
  const monthButton = document.querySelector(`[aria-controls="${month}"]`);

  monthCollapsible.classList.add("show");
  monthButton.classList.remove("collapsed");
  monthButton.ariaExpanded = "true";

  const dateCollapsible = document.getElementById(`${month}-${day}-content`);
  const dateButton = document.querySelector(`[aria-controls="${month}-${day}"]`);

  dateCollapsible.classList.add("show");
  dateButton.classList.remove("collapsed");
  dateButton.ariaExpanded = "true";

  dateButton.scrollIntoView();
}

function onHashChange() {
  const parsedHash = parseHash(location.hash);
  if (parsedHash === undefined) {
    return;
  }

  const [month, day] = parsedHash;

  // this skips the show animation of an element
  const monthCollapsible = document.getElementById(month);
  const bootstrapMonthCollapsible = bootstrap.Collapse.getOrCreateInstance(monthCollapsible, {toggle: false});
  bootstrapMonthCollapsible.show();

  const dateCollapsible = document.getElementById(`${month}-${day}-content`);
  const dateButton = document.querySelector(`[aria-controls="${month}-${day}"]`);
  const bootstrapDateCollapsible = bootstrap.Collapse.getOrCreateInstance(dateCollapsible, {toggle: false});
  bootstrapDateCollapsible.show();

  dateButton.scrollIntoView();
}


function onMonthCollapse(event, monthCollapsible) {
  // apparently the event listener can also receive events for descendents?
  // stackoverflow says the proper way to do this is to add another event 
  // listener to the descendent and call e.stopPropagation() from there but
  // that's too hard

  // this code should really only fire for the month collapsible so let's 
  // insta-return if the target isn't the orignal
  
  const month = monthCollapsible.id;
  const days_in_month = MONTHS.get(month);

  if (event.target != monthCollapsible) {
    return;
  }

  for (let day = 1; day <= days_in_month; day++) {
    const dateCollapsible = document.getElementById(`${month}-${day}-content`);
    const bootstrapDateCollapsible = bootstrap.Collapse.getOrCreateInstance(dateCollapsible, {toggle: false});

    // i don't think we're supposed to use this but it works
    // see also `dateCollapsible.classList.contains("show")`
    if (bootstrapDateCollapsible._isShown()) {
      bootstrapDateCollapsible.hide();
    }
  }
}

function createBirthdays(showLinkCount) {
  const birthdayElements = document.getElementsByClassName("birthday");
  while (birthdayElements.length > 0) {
    birthdayElements[0].remove();
  }

  for (birthday of BIRTHDAYS) {
    const newBirthdayElement = document.createElement("p");
    newBirthdayElement.classList.add("birthday");
    
    const [pageTitle, date, year, description, linkCount] = birthday.values();
    newBirthdayElement.dataset.pageTitle = pageTitle;
    newBirthdayElement.dataset.date = date;
    newBirthdayElement.dataset.year = year;
    newBirthdayElement.dataset.description = description;
    newBirthdayElement.dataset.linkCount = linkCount;

    if (minimumLinks > parseInt(linkCount)) {
      continue;
    }

    const link = document.createElement("a");
    link.setAttribute("href", `https://en.wikipedia.org/wiki/${pageTitle}`);
    link.textContent = "wikipedia link";

    newBirthdayElement.append(`${year} - ${description} (`, link, `)`); 

    const dateCollapsibleBody = document.querySelector(`#${birthday.get("date")}-content > .accordion-body`);
    dateCollapsibleBody.append(newBirthdayElement);
  }

  changeBirthdayVisibility(1000);
}

function changeBirthdayVisibility(minimumLinks) {
  const birthdayElements = document.getElementsByClassName("birthday");

  for (birthday of birthdayElements) {
    if (isNaN(birthday.dataset.linkCount) || parseInt(birthday.dataset.linkCount) < minimumLinks) {
      birthday.classList.add("d-none");
    }
    else {
      birthday.classList.remove("d-none");
    }
  }
}

window.addEventListener("hashchange", onHashChange);
window.addEventListener("DOMContentLoaded", () => {
  createBirthdays();
  loadFromHash();
});

const minimumLinksInput = document.getElementById("minimumLinks");
minimumLinksInput.addEventListener("input", event => {
  const minimumLinks = parseInt(event.target.value);
  if (isNaN(minimumLinks)) {
    return;
  }

  changeBirthdayVisibility(minimumLinks);
});

for (const month of MONTHS.keys()) {
  const monthCollapsible = document.getElementById(month);

  monthCollapsible.addEventListener("hide.bs.collapse", event => onMonthCollapse(event, monthCollapsible));
}
